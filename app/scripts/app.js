import svg4everybody from 'svg4everybody';
// import $ from 'jquery';

$(() => {
	svg4everybody();

	// Anchors
	$('a[href^=#anchor-]').on('click', function (e) {
		var $this = $(this);
		var $target = $(this.hash);
		$target = $target.length ? $target : $('[name=' + this.hash.slice(1) + ']');

		if ($target.length) {
			var offset = parseInt($this.data('offset'), 10) || 0;
			var duration = parseInt($this.data('duration'), 10) || 250;

			$('html, body').animate({
				scrollTop: $target.offset().top - offset
			}, duration);

			e.preventDefault();
		}
	});

	// Google Maps
	window.initMap = function () {
		var myLatLng = {
			lat: 55.5840000,
			lng: 37.1754705
		};
		var map = new google.maps.Map(document.getElementById('map'), {
			center: myLatLng,
			zoom: 13,
			scrollwheel: false,
			disableDefaultUI: true
		});
		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map
		});
	}

	$('.js-super-banner-carousel').owlCarousel({
		lazyLoad: true,
		dots: true,
		nav: false,
		items: 1,
		loop: true,
		autoplay: true,
		mouseDrag: false,
		touchDrag: false,
		animateOut: 'fadeOut'
	});

});
